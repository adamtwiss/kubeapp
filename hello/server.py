from klein import run, route

@route('/')
def home(request):
    return 'Hello, world!\n'

run("0", 8080)
