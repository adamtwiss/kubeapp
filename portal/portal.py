#!/usr/bin/env python

from klein import run, route
import json
import treq
import os
import sys
from twisted.internet.defer import inlineCallbacks, returnValue

PROVISIONING_URL = os.getenv("PROVISIONING_URL") or "http://provisioning"

@route("/")
@inlineCallbacks
def list_sites(request):
    r = yield treq.get(PROVISIONING_URL+"/api/site")
    data = yield r.content()
    data = json.loads(data)
    content = "<h1>Sites</h1><ul>"
    for key,value in data.items():
        print key,value
        content += "<li><a href='/site/%s'>%s</a>" % (key,value)
    content += "</ul>"
    returnValue(content)


@route('/site/<int:id>')
@inlineCallbacks
def site_info(request, id):
    r = yield treq.get(PROVISIONING_URL+"/api/site/%d"%id)
    data = yield r.content()
    data = json.loads(data)
    print "DATA", data
    content = "<h1>Site Info</h1><ul>"
    for key,value in data.items():
        print key,value
        content += "<li>%s:%s</a>" % (key,value)
    content += "</ul>"
    returnValue(content)

PORT=8080
if len(sys.argv)>1:
    PORT=int(sys.argv[1])

run("0", PORT)
