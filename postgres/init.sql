create database site;
\c site;
create table site (id int, name text, url text, origin text);
insert into site  values (1,'google','http://google.com/','http://origin.google.com');
insert into site  values (2,'apple','http://apple.com/','http://origin.apple.com');
insert into site  values (3,'microsoft','http://www.microsoft.com/','http://home.billgates.com/');
