#!/usr/bin/env python

from klein import run, route
import json
import psycopg2
import psycopg2.extras


@route('/')
def home_page(request):
    # Keep LB HC happy
    return "API"

@route('/api/site')
def all_sites(request):
    " Return a dict of { id: name } "
    conn=psycopg2.connect(host="postgres", dbname="site", user="postgres")
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select id, name from site;")
    rows = cursor.fetchall()
    data = {}
    for site in rows:
        data[site['id']] = site['name']
    return json.dumps(data)


@route('/api/site/<int:id>')
def site_info(request, id):
    conn=psycopg2.connect(host="postgres", dbname="site", user="postgres")
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select * from site where id=%s", (id,))
    site = cursor.fetchone()
    return json.dumps(dict(site))

run("0", 8080)
